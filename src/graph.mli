(**************************************************************************)
(*                                                                        *)
(*  Ocamlviz --- real-time profiling tools for Objective Caml             *)
(*  Copyright (C) by INRIA - CNRS - Universite Paris Sud                  *)
(*  Authors: Julien Robert                                                *)
(*           Guillaume Von Tokarski                                       *)
(*           Sylvain Conchon                                              *)
(*           Jean-Christophe Filliatre                                    *)
(*           Fabrice Le Fessant                                           *)
(*  GNU Library General Public License version 2                          *)
(*  See file LICENSE for details                                          *)
(*                                                                        *)
(**************************************************************************)

(** A module that builds a graph on a Lablgtk drawing area *)

(** The kind of the datas represented on the graph *)
type kind = 
    Undefined 
  | Int 
  | Float 
  | Byte 
  | Percentage

(** The way a point is drawn on the graph *)
type style = 
    Full 
  | Dashed 
  | Empty

type t
  (** The abstract type for a graph. *)

type elt
  (** The abstract type for a function of the graph. *)


val create : 
  ?width:int -> ?height:int -> ?timeout:int ->
  ?steps:int -> ?grid_divx:int -> ?grid_divy:int -> ?lborder:float -> 
  ?rborder:float -> ?vborder:float ->
  ?xaxis:int -> ?yaxis:int ->
  ?packing:(GObj.widget -> unit) -> ?kind:kind -> unit -> t
  (** [create ()] creates an empty graph that is a GMisc.drawing_area.
      @param width Widget's width
      @param heigh Widget's height
      @param timeout Graph refresh period (in milliseconds).
      @param steps The number of values displayed on the graph
      @param grid_divx The number of divisions of the background along the X-axis
      @param grid_divy The number of divisions of the background along the Y-axis
      @param lborder The size of the left border of the widget
      @param rborder The size of the right border of the widget
      @param vborder The size of the two borders above and below the drawing area
      @param xaxis The frequency of the X-axis legend (1 = always displayed, 2 = half displayed etc...)
      @param yaxis The frequency of the Y-axis legend
      @param packing The widget to which the graph is packed
      @param kind The kind of the graph
  *)

val add : t -> Protocol.tag -> ?name:string -> (unit -> float * style ) -> kind -> (float * float * float) -> elt
  (** [add g tag n f k c] adds a function [f] with name [n] to the graph [g] and returns it as an element of this graph.
      The function type of the graph is [k]. The function will be displayed with the color [c]
      which is a rgb color with the floating-point numbers spread from 0.0 to 1.0.
      @param name Function name
  *)

val remove : t -> elt -> unit
  (** [remove g f] removes the function [f] from the graph [g]. *)


val set_visible : elt -> bool -> unit
  (** [set_visible f b] displays the function f on the graph according to [b]. *)


val set_preferences : t -> step:int -> divx:int -> divy:int -> xaxis:int -> yaxis:int -> unit
  (** [set_preferences g step divx divy xaxis yaxis ()] changes the value of these arguments 
      in the graph [g].
      @param step The number of values displayed on the graph 
      @param divx The number of divisions of the background along the X-axis
      @param divy The number of divisions of the background along the Y-axis 
      @param xaxis The frequency of the X-axis legend (1 = always displayed, 2 = half displayed etc...)
      @param yaxis The frequency of the Y-axis legend
  *)

val set_color : elt -> ( float * float * float ) -> unit
  (** [set_color f c] sets the color [c] used to display the function [f]. 
  [c] is a rgb color with the floating-point numbers spread from 0.0 to 1.0. *)

val get_color : elt -> float * float * float
  (** [get_color f] returns the current color used to display the function [f].
      The color is a rgb color with the floating-point numbers spread from 0.0 to 1.0.
  *)

val get_nb_fct : t -> int
  (** [get_nb_fct g] return the number of functions displayed on [g]. *)

val get_kind : t -> kind
  (** [get_kind g] returns the kind of the graph [g]. *)

val stop : t -> unit
  (** [stop g] stops the timer inside the graph [g]. This function is necessary 
     to remove cleanly the graph [g] from a graphical user interface. *)

val resume : t -> unit 
  (** [resume g] restarts the timer of the graph [g] with its old period. *)

val restart : t -> int -> unit 
  (** [restart g p] clears the graph [g] and restarts the timer with the period [p]. *)

val clear : t -> unit 
  (** [clear g] clears the graph. *)

val set_timer : t -> int -> unit
  (** [set_timer g t] removes the timer inside [g] and replaces it with 
      a new timer whose new timer period will be [t] (in milliseconds).
  *)

val get_timer : t -> GMain.Timeout.id option
  (** [get_timer g] returns the timer inside [g]. *)

val get_period : t -> int
  (** [get_period g] returns the value of the timer inside [g] (in milliseconds). *)

val get_steps : t -> int
  (** [get_steps g] returns the number of values that are displayed in the graph [g] for each function. 
  *)

val get_gridx : t -> int
  (** [get_gridx g] returns the number of divisions of the background along the X-axis
      on the graph [g].
  *)

val get_gridy : t -> int
  (** [get_gridy g] returns the number of divisions of the background along the Y-axis
      on the graph [g].
  *)

val get_xaxis : t -> int
  (** [get_xaxis g] returns the frequency of the X-axis legend of the graph [g]
      (1 = always displayed, 2 = half displayed etc...) *)

val get_yaxis : t -> int
  (** [get_yaxis g] returns the frequency of the Y-axis legend of the graph [g]
      (1 = always displayed, 2 = half displayed etc...) *)




(* val go_tp : t -> float -> unit *)


(*
Local Variables: 
compile-command: "unset LANG; make -C .."
End: 
*)

