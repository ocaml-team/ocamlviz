(**************************************************************************)
(*                                                                        *)
(*  Ocamlviz --- real-time profiling tools for Objective Caml             *)
(*  Copyright (C) by INRIA - CNRS - Universite Paris Sud                  *)
(*  Authors: Julien Robert                                                *)
(*           Guillaume Von Tokarski                                       *)
(*           Sylvain Conchon                                              *)
(*           Jean-Christophe Filliatre                                    *)
(*           Fabrice Le Fessant                                           *)
(*  GNU Library General Public License version 2                          *)
(*  See file LICENSE for details                                          *)
(*                                                                        *)
(**************************************************************************)



type column = Name 
	      | Tag_count 
	      | Tag_size 
	      | Tag_max_count 
	      | Tag_max_size 
	      | Tag_percent_size 
	      | Stat_count 
	      | Stat_time 
	      | Stat_percent_time 
	      | Value_i 
	      | Value_f 
	      | Value_b 
	      | Value_s 
	      | Gc_total 
	      |	Gc_max 
	      | Gc_alive 
	      | Hash_elts
	      | Hash_size
	      | Hash_bckts
	      | Hash_empty_bckts
	      | Hash_mean
	      | Hash_percent
	      | Null 

type page = Stat 
	    | Gc 
	    | Tag 
	    | Value 
	    | Value_int 
	    | Value_float 
	    | Value_bool 
	    | Value_string 
	    | Exported_graph of column * GMenu.menu_item 
	    | Exported_list of GMenu.menu_item 
	    | Tree
	    | Hash
            | Log


(* Converts a string to percent *)
let to_percent s = 
    let l = String.length s in
    Format.sprintf "%s %%" (String.sub s 0 (if l > 4 then 4 else l))


(* Converts an int64 to a "byte" string *)
let string_of_byte b = 
  let temp b x =
    let s = Int64.to_string b in
    let l = String.length s in
    let s1 = String.sub s 0 (l-x) in
    let s2 = String.sub s (l-x) 3 in
    Format.sprintf "%s.%s" s1 s2 
  in
  if b > (Int64.of_int 1000000000) then
    Format.sprintf "%s GB" (temp b 9)
  else if b > (Int64.of_int 1000000) then
    Format.sprintf "%s MB" (temp b 6)
  else if b > (Int64.of_int 1000) then
    Format.sprintf "%s KB" (temp b 3)
  else let s = Int64.to_string b in Format.sprintf "%s B" s


let time_of_float (f: float) = 
  if f < 60. then 
    let s = string_of_float f in
    let len = String.length s in
    Format.sprintf "%s sec" (String.sub s 0 (if len < 5 then len else 5))
  else 
    if f < 3600. then 
      begin
	let f' = int_of_float f in
	  let sec = f' mod 60 in
	  let min = f' /60 in
	  Format.sprintf "%s min %s sec" (string_of_int min) (string_of_int sec)
      end
    else 
      begin
	let f' = int_of_float f in
	let sec = f' mod 60 in
	let min = f' mod 3600 /60 in
	let h = f' / 3600 in 
	Format.sprintf "%s h %s min %s sec" (string_of_int h) (string_of_int min) (string_of_int sec)
      end
  

let time_of_float_parenthesis f = 
  let s = time_of_float f in 
  Format.sprintf "  (%s)" s


 (*
Local Variables: 
compile-command: "unset LANG; make -C .."
End: 
*)
