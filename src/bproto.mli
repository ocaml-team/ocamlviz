(**************************************************************************)
(*                                                                        *)
(*  Ocamlviz --- real-time profiling tools for Objective Caml             *)
(*  Copyright (C) by INRIA - CNRS - Universite Paris Sud                  *)
(*  Authors: Julien Robert                                                *)
(*           Guillaume Von Tokarski                                       *)
(*           Sylvain Conchon                                              *)
(*           Jean-Christophe Filliatre                                    *)
(*           Fabrice Le Fessant                                           *)
(*  GNU Library General Public License version 2                          *)
(*  See file LICENSE for details                                          *)
(*                                                                        *)
(**************************************************************************)

(** Binary implementation of the protocol *)

val encode_one : Buffer.t -> Protocol.msg -> unit
  (** [encode_one b m] encodes a message [m] in the buffer [b]. *)

val encode : Buffer.t -> Protocol.msg list -> unit
  (** [encode b ml] encodes a message list [ml] in the buffer [b].
  *)

val decode_one : string -> int -> int -> Protocol.msg * int
  (** [decode s first last] decodes messages using
      characters in [s] from [first] to [last];
      returns the messages together with the position right after
      the last decoded message.
  *)
      

(** DEBUG *)
val dump : string -> string -> int -> int -> unit

(*
Local Variables: 
compile-command: "unset LANG; make -C .."
End: 
*)

