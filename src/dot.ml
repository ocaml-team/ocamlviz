(**************************************************************************)
(*                                                                        *)
(*  Ocamlviz --- real-time profiling tools for Objective Caml             *)
(*  Copyright (C) by INRIA - CNRS - Universite Paris Sud                  *)
(*  Authors: Julien Robert                                                *)
(*           Guillaume Von Tokarski                                       *)
(*           Sylvain Conchon                                              *)
(*           Jean-Christophe Filliatre                                    *)
(*           Fabrice Le Fessant                                           *)
(*  GNU Library General Public License version 2                          *)
(*  See file LICENSE for details                                          *)
(*                                                                        *)
(**************************************************************************)

open Format
open Protocol

module H = 
  Hashtbl.Make(struct 
		 type t = variant 
		 let equal = (==) 
		 let hash = Hashtbl.hash end)

let color v l =
  if List.memq v l then " color=\"chartreuse3\" style=filled" else ""

let write_to f ?(colored=[]) v =
  let c = open_out f in
  let fmt = formatter_of_out_channel c in
  let h = H.create 17 in
  let idx = ref 0 in
  let rec visit (Node (s, l) as v) =
    try
      H.find h v
    with Not_found ->
      incr idx;
      let n = "node" ^ string_of_int !idx in
      H.add h v n;
      fprintf fmt "  %s [label=\"%s\" %s];@\n" n s (color v colored);
      let l = List.map visit l in
      List.iter (fun n' -> fprintf fmt "%s -> %s;@\n" n n') l;
      n
  in
  fprintf fmt "digraph {@\n";
  ignore (visit v);
  fprintf fmt "}@.";
  close_out c





(*
Local Variables: 
compile-command: "unset LANG; make -C .."
End: 
*)
