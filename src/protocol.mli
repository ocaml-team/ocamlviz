(**************************************************************************)
(*                                                                        *)
(*  Ocamlviz --- real-time profiling tools for Objective Caml             *)
(*  Copyright (C) by INRIA - CNRS - Universite Paris Sud                  *)
(*  Authors: Julien Robert                                                *)
(*           Guillaume Von Tokarski                                       *)
(*           Sylvain Conchon                                              *)
(*           Jean-Christophe Filliatre                                    *)
(*           Fabrice Le Fessant                                           *)
(*  GNU Library General Public License version 2                          *)
(*  See file LICENSE for details                                          *)
(*                                                                        *)
(**************************************************************************)

(** The signature of protocol datas *)

type kind = 
    Point         (*  0  *)
  | Time          (*  1  *)
  | Value_int     (*  2  *)
  | Value_float   (*  3  *)
  | Value_bool    (*  4  *)
  | Value_string  (*  5  *)
  | Tag_count     (*  6  *)
  | Tag_size      (*  7  *)
  | Ktree         (*  9  *)
  | Hash          (*  10 *)  
  | Special (** for the garbage collector stats and total execution time *) (* 8 *)
  | Klog           (*  11 *)
 

(** The type that will be observed in the module Tree. *)
type variant = Node of string * variant list

type value =
    Int of int 
  | Float of float
  | String of string 
  | Bool of bool
  | Int64 of Int64.t 
  | Tree of variant
  | No_value
  | Collected
  | Killed
  | Hashtable of value * value * value * value
  | Log of (float * string) list

type tag = int
    (** A tag is an unique representation of a Ocamlviz data. The tags 0,1,2 are always taken. *)
	    
(** There are 3 sorts of messages: 
    - [Declare] to send tags to new clients
    - [Send] to send values to clients
    - [Bind] to send bindings to new clients*)
type msg = 
  | Declare of tag*kind*string 
  | Send of tag*value
  | Bind of tag list


(*
Local Variables: 
compile-command: "unset LANG; make -C .."
End: 
*)
