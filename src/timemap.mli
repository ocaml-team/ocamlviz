(**************************************************************************)
(*                                                                        *)
(*  Ocamlviz --- real-time profiling tools for Objective Caml             *)
(*  Copyright (C) by INRIA - CNRS - Universite Paris Sud                  *)
(*  Authors: Julien Robert                                                *)
(*           Guillaume Von Tokarski                                       *)
(*           Sylvain Conchon                                              *)
(*           Jean-Christophe Filliatre                                    *)
(*           Fabrice Le Fessant                                           *)
(*  GNU Library General Public License version 2                          *)
(*  See file LICENSE for details                                          *)
(*                                                                        *)
(**************************************************************************)


(** A small module to store datas in an array and retrieve them with a logarithmic complexity. *)

type 'a t

val create : ?size:int -> 'a -> 'a t
  (** [create d] creates a timemap with the data d.
      @param size The size of the timemap at its creation *)

val add : 'a t -> float -> 'a -> unit
  (** [add tm f d] adds a floating-point [f] and a 'a [d] to the timemap [tm]. *)

val find : 'a t -> float -> float * 'a
  (** [find tm t] returns the most recent pair [(t',v')] in [tm] 
      with [t'] smaller or equal than [t];
      raises [Not_found] if [tm] contains no element or if the
      first element of [tm] is younger than [t]. *)

val remove_before : 'a t -> float -> unit
  (** [remove_before tm t] removes all entries younger or equal to [t] in [tm]. *)

val get_min : unit -> float
  (** [get_min ()] returns the index of the minimum 'a in the timemap. *)


