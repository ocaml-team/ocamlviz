(**************************************************************************)
(*                                                                        *)
(*  Ocamlviz --- real-time profiling tools for Objective Caml             *)
(*  Copyright (C) by INRIA - CNRS - Universite Paris Sud                  *)
(*  Authors: Julien Robert                                                *)
(*           Guillaume Von Tokarski                                       *)
(*           Sylvain Conchon                                              *)
(*           Jean-Christophe Filliatre                                    *)
(*           Fabrice Le Fessant                                           *)
(*  GNU Library General Public License version 2                          *)
(*  See file LICENSE for details                                          *)
(*                                                                        *)
(**************************************************************************)

(** the Ocamlviz interface for clients *)
open Protocol

(** The status of a monitored value *)
type status = St_active | St_killed | St_collected


(** This is the client side type used to describe a monitored data . 
    [tmap] is the array containing successive values of the tag [id] over time. 
    [tmap_st] is the equivalent of [tmap] for status. *)
type tag = {
  id:Protocol.tag;
  kind:kind; 
  name:string;
  tmap : value Timemap.t;
  mutable value : float*value;
  tmap_st : status Timemap.t;
  mutable status: float*status;
}

(** {blank_line} *)

val add_tag : Protocol.tag -> kind -> string -> unit
  (** [add_tag id k n] adds to the database a tag whose id is [id], kind [k], and name [n]. *)

val change_value : Protocol.tag -> value -> unit
  (** [change_value id v] replaces the value of the tag whose identifier is [id] by the value [v]. *)


val interp : msg -> unit
  (** [interp m] matches the message [m] and calls [add_tag] if [m] is a [Declare] message, or
      [change_value] if [m] is a [Send] message. *)


val clean_up : unit -> unit
(** [clean_up ()] cleans up every stored values from every timemaps that are older than the record window. *)

(** {blank_line} *)

val get_current_time : unit -> float
(** [get_current_time ()] returns the current time. This is the last time datas were received. *)


val set_observe_time : float -> unit
  (** [set_observe_time t] sets the observation time of the data base at [t]. 
      It is an alternative to [set_offset t']. *)


val get_observe_time : unit -> float
  (** [get_observe_time ()] returns the observation time of the data base. *)


val set_offset : float -> unit
  (** [set_offset t] sets the offset of the database at the time [t], which means that the difference 
      between the observation time and the current time will be [t]. *)


val get_offset : unit -> float
  (** [get_offset ()] returns the difference between observation time and current time. *)


val set_record_window : float -> unit
  (** [set_record_window t] changes the size of the record window with [t] in seconds.
  This means that the database will now record [t] seconds of execution. *)


val get_record_window : unit -> float
(** [get_time_window ()] returns the size of the record window in seconds. *)


val get_log_size : unit -> int 


val set_log_size : int -> unit


val get_tag : Protocol.tag -> tag
  (** [get_tag id] returns the tag whose identifier is [id]. *)


val get_bindings : Protocol.tag -> Protocol.tag list
  (** [get_bindings id] returns a list of tags who are bound together. *)


val get_heap_total_size : unit -> float * value
  (** [get_heap_total_size ()] returns the total size value of the major heap
      that is the closest to the observation time along with its last change time. *)


val get_heap_alive_size : unit -> float * value
  (** [get_heap_alive_size ()] returns the alive datas size value of the major heap
      that is the closest to the observation time along with its last change time. *)


val get_heap_max_size : unit -> float * value
  (** [get_heap_max_size ()] returns the maximum size value of the major heap 
      that is the closest to the observation time along with its last change time. *)


(** {blank_line} *)

(** The following functions return couples (time,[value]) for the given [Protocol.tag]. *)

(** {blank_line} *)

val get_point : Protocol.tag -> float * value
  (** [get_point id] returns the value of the monitored [Point] [id] that is the closest 
      to the observation time along with its last change time. *)


val get_time : Protocol.tag -> float * value
  (** [get_time id] returns the value of the monitored [Time] [id] that is the closest 
      to the observation time along with its last change time. *)


val get_percent_time : Protocol.tag -> float * value
  (** [get_percent_time id] returns the percentage of the monitored [Time] [id], related to the observation time, that is the closest 
      to the observation time. The time returned is the one returned by [get_time id]. *)


val get_tag_size : Protocol.tag -> float * value
  (** [get_tag_size id] returns the size value of the monitored [Tag] [id] that is the closest 
      to the observation time along with its last change time. *)


val get_tag_max_size : Protocol.tag -> float * value
  (** [get_tag_max_size id] returns the maximum size value of the monitored [Tag] [id] that is the closest 
      to the observation time along with its last change time. *)


val get_tag_percent_size : Protocol.tag -> float * value
  (** [get_tag_percent_size id] returns the percentage of the monitored [Tag] [id], related to the major heap size, that is the closest 
      to the observation time. The time returned is the one returned by [get_tag_size id]. *)


val get_tag_count : Protocol.tag -> float * value
  (** [get_tag_count id] returns the cardinal-number value of the monitored [Tag] [id] that is the closest 
      to the observation time along with its last change time. *)


val get_tag_max_count : Protocol.tag -> float * value
  (** [get_tag_max_count id] returns the maximum cardinal-number value of the monitored [Tag] [id] that is the closest 
      to the observation time along with its last change time. *)


val get_value_int : Protocol.tag -> float * value
  (** [get_value_int id] returns the value of the monitored integer [Value] [id] that is the closest 
      to the observation time along with its last change time. *)


val get_value_float : Protocol.tag -> float * value
  (** [get_value_float id] returns the value of the monitored floating-point [Value] [id] that is the closest 
      to the observation time along with its last change time. *)


val get_value_string : Protocol.tag -> float * value
  (** [get_value_string id] returns the value of the monitored string [Value] [id] that is the closest 
      to the observation time along with its last change time. *)


val get_value_bool : Protocol.tag -> float * value
  (** [get_value_bool id] returns the value of the monitored boolean [Value] [id] that is the closest 
      to the observation time along with its last change time. *)


val get_tree : Protocol.tag -> float * value
  (** [get_tree id] returns the value of the monitored [Tree] [id] that is the closest 
      to the observation time along with its last change time. *)


val get_hash : Protocol.tag -> float * value
  (** [get_hash id] returns the value of the monitored [Hashtable] [id] that is the closest 
      to the observation time along with its last change time. *)


val get_hash_percent_filled : Protocol.tag -> float * value
  (** [get_hash_percent_filled id] returns the filling rate of the monitored [Hashtable] [id] that is the closest 
      to the observation time. The time returned is the one returned by [get_hash id]. *)


val get_hash_mean : Protocol.tag -> float * value
  (** [get_hash_mean id] returns the buckets' mean length of the monitored [Hashtable] [id] that is the closest 
      to the observation time. The time returned is the one returned by [get_hash id]. *)



(* LISTS *)
(** {blank_line} *)

(** The following functions return lists of value of a certain [kind]. *)

(** {blank_line} *)

val get_point_list: unit -> tag list
  (** [get_point_list ()] returns a list of every tags whose [kind] is Point. *)


val get_time_list: unit -> tag list
  (** [get_time_list ()] returns a list of every tags whose [kind] is Time. *)


val get_tag_count_list: unit -> tag list
  (** [get_tag_count_list ()] returns a list of every tags whose [kind] is Tag_count. *)


val get_tag_size_list: unit -> tag list
  (** [get_tag_size_list ()] returns a list of every tags whose [kind] is Tag_size. *)


val get_value_int_list : unit -> tag list
  (** [get_value_int_list ()] returns a list of every tags whose [kind] is Value_int. *)


val get_value_float_list : unit -> tag list
  (** [get_value_float_list ()] returns a list of every tags whose [kind] is Value_float. *)


val get_value_string_list : unit -> tag list
  (** [get_value_string_list ()] returns a list of every tags whose [kind] is Value_string. *)


val get_value_bool_list : unit -> tag list
  (** [get_value_bool_list ()] returns a list of every tags whose [kind] is Value_bool. *)
  

val get_tree_list : unit -> tag list
  (** [get_tree_list ()] returns a list of every tags whose [kind] is Ktree. *)
  

val get_hash_list : unit -> tag list
  (** [get_hash_list ()] returns a list of every tags whose [kind] is Hash. *)


val get_log : unit -> float * value



(*
Local Variables: 
compile-command: "unset LANG; make -C .."
End: 
*)
