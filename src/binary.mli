(**************************************************************************)
(*                                                                        *)
(*  Ocamlviz --- real-time profiling tools for Objective Caml             *)
(*  Copyright (C) by INRIA - CNRS - Universite Paris Sud                  *)
(*  Authors: Julien Robert                                                *)
(*           Guillaume Von Tokarski                                       *)
(*           Sylvain Conchon                                              *)
(*           Jean-Christophe Filliatre                                    *)
(*           Fabrice Le Fessant                                           *)
(*  GNU Library General Public License version 2                          *)
(*  See file LICENSE for details                                          *)
(*                                                                        *)
(**************************************************************************)

(** Low-level functions for the binary protocol. *)


(** Coding functions *)
(** {blank_line} *)

val buf_int8 :  Buffer.t -> int -> unit
(** [buf_int8 b i] encodes a 8-bits integer [i] in the buffer [b]. *)

val buf_int16 :  Buffer.t -> int -> unit
  (** [buf_int16 b i] encodes a 16-bits integer [i] in the buffer [b]. *)

val buf_int31 :  Buffer.t -> int -> unit
  (** [buf_int31 b i] encodes a native 31-bits integer [i] in the buffer [b]. *)

val buf_int63 : Buffer.t -> int -> unit
  (** [buf_int63 b i] encodes a native 63-bits integer [i] in the buffer [b]. *)

val buf_string31 :  Buffer.t -> string -> unit

val buf_bool :  Buffer.t -> bool -> unit
  (** [buf_bool b i] encodes a boolean [i] in the buffer [b]. *)

val buf_float : Buffer.t -> float -> unit
  (** [buf_float b f] encodes a floating-point number [f] in the buffer [b]. *)

val buf_int64 : Buffer.t -> Int64.t -> unit
  (** [buf_int64 b i] encodes an int64 [i] in the buffer [b]. *)

val buf_list16 : (Buffer.t -> 'a -> unit) -> Buffer.t -> 'a list -> unit
  (** [buf_list16 f b l] encodes the list [l] in the buffer [b], with the function [f] specifying how to encode an item of [l]. *)


exception IncompleteMessage
(** {blank_line} *)

(** Decoding functions *)

(** {blank_line} *)

val get_uint8 : string -> int -> int
  (** [get_uint8 s pos] decodes a byte in [s] at the position [pos]. *)

val get_int16 : string -> int -> int * int
  (** [get_int16 s pos] decodes two bytes in [s] at the position [pos] 
      and return a 16 bits integer along with the new position in [s]. *)

val get_int31 : string -> int -> int * int
  (** [get_int31 s pos] decodes four bytes in [s] at the position [pos] 
      and return a 31 bits integer along with the new position in [s]. *)

val get_int63 : string -> int -> int * int
  (** [get_int63 s pos] decodes eight bytes in [s] at the position [pos] 
      and return a 63 bits integer along with the new position in [s]. *)

val get_int64 : string -> int -> Int64.t * int
  (** [get_int64 s pos] decodes eight bytes in [s] at the position [pos] 
      and return a 63 bits integer along with the new position in [s]. *)

val get_int63_of_31 : string -> int -> int * int
  (** [get_int63_of_31 s pos] decodes four bytes in [s] at the position [pos] 
      and return a 63 bits integer along with the new position in [s], made from the 31 bits integer. *)

val get_int64_of_63 : string -> int -> Int64.t * int
  (** [get_int64_of_63 s pos] decodes eight bytes in [s] at the position [pos] 
      and return a 64 bits integer along with the new position in [s], made from the 63 bits integer. *)

val get_float : string -> int -> float * int
  (** [get_float s pos] decodes eight bytes in [s] at the position [pos] 
      and return a floating-point number along with the new position in [s]. *)

val get_string : string -> int -> int -> string * int
  (** [get_string s pos len] decodes [len] bytes in [s] at the position [pos] 
      and return a string along with the new position in [s]. *)

val get_list16 : (string -> int -> int -> 'b * int) -> string -> int -> int -> 'b list * int
  (** [get_list16 f s pos last] decodes [last]-[pos] bytes in [s], at the position [pos],
      with the function [f] that specifies how to get an item, and returns a 'b list. *)

val get_bool : string -> int -> bool
  (** [get_bool s pos] decodes a byte in [s] at the position [pos] 
      and return a boolean along with the new position in [s]. *)

  

(*
Local Variables: 
compile-command: "unset LANG; make -C .."
End: 
*)
