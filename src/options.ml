let machine = ref ""
let port = ref 51000
let output = ref "ascii.log"
let terminal = ref false

let () =
  Arg.parse 
    ["-server", Arg.Set_string machine, "<machine>  specifies the server machine";
     "-port", Arg.Set_int port, "<port>  specifies the server port";
     "-o", Arg.Set_string output, "<output> specifies the name of the generated file";
     "-t", Arg.Set terminal, "<terminal> displays the content of the file in a terminal"]
    (fun _ -> ())
    "usage: client [options]"

