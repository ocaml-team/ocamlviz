(**************************************************************************)
(*                                                                        *)
(*  Ocamlviz --- real-time profiling tools for Objective Caml             *)
(*  Copyright (C) by INRIA - CNRS - Universite Paris Sud                  *)
(*  Authors: Julien Robert                                                *)
(*           Guillaume Von Tokarski                                       *)
(*           Sylvain Conchon                                              *)
(*           Jean-Christophe Filliatre                                    *)
(*           Fabrice Le Fessant                                           *)
(*  GNU Library General Public License version 2                          *)
(*  See file LICENSE for details                                          *)
(*                                                                        *)
(**************************************************************************)

(** Library to instrument the monitored program (when using a thread server) *)

include Monitor_sig.Monitor

val yield : unit -> unit
  (** Use this function to allow monitoring in computation-only code. *)

val send_now : unit -> unit
  (** [send_now ()] immediatly sends the current values of monitored datas to the clients. *)

val init : unit -> unit
  (** [init ()] has to be used to initialize Ocamlviz_threads. *)
