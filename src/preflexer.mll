(**************************************************************************)
(*                                                                        *)
(*  Ocamlviz --- real-time profiling tools for Objective Caml             *)
(*  Copyright (C) by INRIA - CNRS - Universite Paris Sud                  *)
(*  Authors: Julien Robert                                                *)
(*           Guillaume Von Tokarski                                       *)
(*           Sylvain Conchon                                              *)
(*           Jean-Christophe Filliatre                                    *)
(*           Fabrice Le Fessant                                           *)
(*  GNU Library General Public License version 2                          *)
(*  See file LICENSE for details                                          *)
(*                                                                        *)
(**************************************************************************)

{
  let steps = ref 0
  let gridx = ref 0
  let gridy = ref 0
  let xaxis = ref 0
  let yaxis = ref 0
  let period = ref 0
  let record = ref 0.
  let log = ref 0
}

let space = [' ' '\t' '\n']
let integer = ['0'-'9']+
let number = '-'? ['0'-'9']+
    |'-'? ['0'-'9']+ '.' ['0'-'9']* 
    | '-'? ['0'-'9']* '.' ['0'-'9']+


rule next_data = parse
  | space+ { next_data lexbuf }

  |'\n' { next_data lexbuf }

  |"steps" space+ (integer as n) space+
      { steps := int_of_string n; next_data lexbuf }

  |"gridx" space+ (integer as n) space+ 
      { gridx := int_of_string n; next_data lexbuf }

  |"gridy" space+ (integer as n) space+ 
      { gridy := int_of_string n; next_data lexbuf }

  |"xaxis" space+ (integer as n) space+ 
      { xaxis := int_of_string n; next_data lexbuf }

  |"yaxis" space+ (integer as n) space+ 
      { yaxis := int_of_string n; next_data lexbuf }

  |"period" space+ (integer as n) space+ 
      { period := int_of_string n; next_data lexbuf }

  |"record" space+ (number as n) space+  
      { record := float_of_string n; next_data lexbuf }

  |"log" space+ (integer as n) space+  
      { log := int_of_string n; next_data lexbuf }

  | eof
      { }


{ 

  let read_file file =
    try
      let f = open_in file in
      let l = Lexing.from_channel f in
      next_data l;
      close_in f;
      (!steps,!gridx,!gridy,!xaxis,!yaxis,!period,!record,!log)
    with Sys_error _ -> (500,5,5,1,1,100,60.,200)
}


(*
Local Variables: 
compile-command: "unset LANG; make -C .."
End: 
*)
