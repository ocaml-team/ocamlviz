(**************************************************************************)
(*                                                                        *)
(*  Ocamlviz --- real-time profiling tools for Objective Caml             *)
(*  Copyright (C) by INRIA - CNRS - Universite Paris Sud                  *)
(*  Authors: Julien Robert                                                *)
(*           Guillaume Von Tokarski                                       *)
(*           Sylvain Conchon                                              *)
(*           Jean-Christophe Filliatre                                    *)
(*           Fabrice Le Fessant                                           *)
(*  GNU Library General Public License version 2                          *)
(*  See file LICENSE for details                                          *)
(*                                                                        *)
(**************************************************************************)

open Ocamlviz

let swap a i j = let t = a.(i) in a.(i) <- a.(j); a.(j) <- t

let p = Point.create "p"

let time_quicksort = Time.create "quicksort"

let quicksort a =
  let rec quick_rec l r =
    (* Ocamlviz.send_now (); *)
    if l < r then begin
      let v = a.(l) in
      let m = ref l in
      for i = l+1 to r do if a.(i) < v then begin incr m; swap a i !m end done;
      swap a l !m;
      quick_rec l (!m-1);
      quick_rec (!m+1) r
    end
  in
  quick_rec 0 (Array.length a - 1)

let quicksort a =
  Time.start time_quicksort;
  quicksort a;
  Time.stop time_quicksort

let () = wait_for_connected_clients 1

let () =
  for i = 1 to 10 do
    let a = Array.init 3_000_000 (fun _ -> Random.int 3000000) in
    quicksort a 
  done

