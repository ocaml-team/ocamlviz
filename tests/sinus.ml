(**************************************************************************)
(*                                                                        *)
(*  Ocamlviz --- real-time profiling tools for Objective Caml             *)
(*  Copyright (C) by INRIA - CNRS - Universite Paris Sud                  *)
(*  Authors: Julien Robert                                                *)
(*           Guillaume Von Tokarski                                       *)
(*           Sylvain Conchon                                              *)
(*           Jean-Christophe Filliatre                                    *)
(*           Fabrice Le Fessant                                           *)
(*  GNU Library General Public License version 2                          *)
(*  See file LICENSE for details                                          *)
(*                                                                        *)
(**************************************************************************)

open Ocamlviz


let () = 
  init ();
  wait_for_connected_clients 1;
  let t = ref 0 in
  let x = Value.observe_int_ref  "my value" (ref 1) in
  let l = ref [] in
  for i = 0 to 100_000_000 do
    yield ();
    incr t;
    l := !t :: !l;
    l := List.rev !l;
    x := truncate (50. +. 50. *. sin (float !t *. 0.001));
  done;



(*
Local Variables: 
compile-command: "unset LANG; make -C .. test-sinus"
End: 
*)
