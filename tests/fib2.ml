(**************************************************************************)
(*                                                                        *)
(*  Ocamlviz --- real-time profiling tools for Objective Caml             *)
(*  Copyright (C) by INRIA - CNRS - Universite Paris Sud                  *)
(*  Authors: Julien Robert                                                *)
(*           Guillaume Von Tokarski                                       *)
(*           Sylvain Conchon                                              *)
(*           Jean-Christophe Filliatre                                    *)
(*           Fabrice Le Fessant                                           *)
(*  GNU Library General Public License version 2                          *)
(*  See file LICENSE for details                                          *)
(*                                                                        *)
(**************************************************************************)

open Ocamlviz

let p1 = Point.create "p"

let fib n = 
  let rec fib_aux n =
    Point.observe p1;
    
    if n <= 1 then 
      1
    else
      fib_aux (n-1) + fib_aux (n-2)
  in
  fib_aux n


let _ = 
  init ();
  wait_for_connected_clients 1;
  (Time.time "t" fib) 100



(*
Local Variables: 
compile-command: "unset LANG; make -C .. test-fib2-byte"
End: 
*)
