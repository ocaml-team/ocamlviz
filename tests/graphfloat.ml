(**************************************************************************)
(*                                                                        *)
(*  Ocamlviz --- real-time profiling tools for Objective Caml             *)
(*  Copyright (C) by INRIA - CNRS - Universite Paris Sud                  *)
(*  Authors: Julien Robert                                                *)
(*           Guillaume Von Tokarski                                       *)
(*           Sylvain Conchon                                              *)
(*           Jean-Christophe Filliatre                                    *)
(*           Fabrice Le Fessant                                           *)
(*  GNU Library General Public License version 2                          *)
(*  See file LICENSE for details                                          *)
(*                                                                        *)
(**************************************************************************)

open Ocamlviz

let float_test = Value.observe_float_ref  "float_test" (ref 0.)
  
let () = 
  init ();
  while true do
    float_test := !float_test +. (Random.float 2.);
    log "%f @." !float_test;
    Unix.sleep 1
  done;
 





(*
Local Variables: 
compile-command: "unset LANG; make -C .. test-graphfloat"
End: 
*)
