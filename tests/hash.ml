(**************************************************************************)
(*                                                                        *)
(*  Ocamlviz --- real-time profiling tools for Objective Caml             *)
(*  Copyright (C) by INRIA - CNRS - Universite Paris Sud                  *)
(*  Authors: Julien Robert                                                *)
(*           Guillaume Von Tokarski                                       *)
(*           Sylvain Conchon                                              *)
(*           Jean-Christophe Filliatre                                    *)
(*           Fabrice Le Fessant                                           *)
(*  GNU Library General Public License version 2                          *)
(*  See file LICENSE for details                                          *)
(*                                                                        *)
(**************************************************************************)

open Ocamlviz

let h = Hashtable.observe "h" (Hashtbl.create 17)
let h2 = Hashtable.observe "h2" ~period:1000 (Hashtbl.create 17)

let x = ref 0

let () = 
  init ();
  while true do 
    if !x mod 100 = 0 then
      begin
	Hashtbl.add h (Random.int 2000) 2; 
	Hashtbl.add h2 !x !x;
      end;
    incr x;
    let rec fct i =
      if i > 0 then fct (i-1)
      else ()
    in
    fct 2000;
  done;
      




(*
  Local Variables: 
  compile-command: "unset LANG; make -C .. test-hash"
  End: 
*)
