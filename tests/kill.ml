(**************************************************************************)
(*                                                                        *)
(*  Ocamlviz --- real-time profiling tools for Objective Caml             *)
(*  Copyright (C) by INRIA - CNRS - Universite Paris Sud                  *)
(*  Authors: Julien Robert                                                *)
(*           Guillaume Von Tokarski                                       *)
(*           Sylvain Conchon                                              *)
(*           Jean-Christophe Filliatre                                    *)
(*           Fabrice Le Fessant                                           *)
(*  GNU Library General Public License version 2                          *)
(*  See file LICENSE for details                                          *)
(*                                                                        *)
(**************************************************************************)

open Ocamlviz

let point = Point.create "point"
let time = Time.create "time"
let tag = Tag.create "tag"

let () =
  init ();
  wait_for_connected_clients 1;
  let l = Tag.mark tag (ref []) in
  Time.start time;
  for n = 1 to 1_000_000 do
    Point.observe point;
    l := n :: !l;
  done;
  Time.stop time;
  Point.kill point;
  Time.kill time;
  Tag.kill tag;
  wait_for_killed_clients ()

(*
Local Variables: 
compile-command: "unset LANG; make -C .. test-kill"
End: 
*)

