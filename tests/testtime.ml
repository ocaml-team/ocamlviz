(**************************************************************************)
(*                                                                        *)
(*  Ocamlviz --- real-time profiling tools for Objective Caml             *)
(*  Copyright (C) by INRIA - CNRS - Universite Paris Sud                  *)
(*  Authors: Julien Robert                                                *)
(*           Guillaume Von Tokarski                                       *)
(*           Sylvain Conchon                                              *)
(*           Jean-Christophe Filliatre                                    *)
(*           Fabrice Le Fessant                                           *)
(*  GNU Library General Public License version 2                          *)
(*  See file LICENSE for details                                          *)
(*                                                                        *)
(**************************************************************************)

open Ocamlviz

let time = Time.create "time"


let rec f x y acc = 
  if x > 0. then f (x-.1.) y (x *. y +. acc)
  else acc

let g () = 
  Time.start time;
  ignore (f 500000000. 5. 0.);
  Time.stop time

let () = Format.eprintf "1@."; wait_for_connected_clients 1

let () = Format.eprintf "2@."; g ()

let () = Format.eprintf "3@."; g ()

let () = Format.eprintf "4@."; ignore(f 500000000. 5. 0.)

let () = Format.eprintf "5@."; g ()

let () = Format.eprintf "6@."; wait_for_killed_clients ()


(*
Local Variables: 
compile-command: "unset LANG; make -C .. test-testtime"
End: 
*)
