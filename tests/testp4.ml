(**************************************************************************)
(*                                                                        *)
(*  Ocamlviz --- real-time profiling tools for Objective Caml             *)
(*  Copyright (C) by INRIA - CNRS - Universite Paris Sud                  *)
(*  Authors: Julien Robert                                                *)
(*           Guillaume Von Tokarski                                       *)
(*           Sylvain Conchon                                              *)
(*           Jean-Christophe Filliatre                                    *)
(*           Fabrice Le Fessant                                           *)
(*  GNU Library General Public License version 2                          *)
(*  See file LICENSE for details                                          *)
(*                                                                        *)
(**************************************************************************)

let laref = ref 0

let laref = ref 0.

let laref = ref ""

let laref = ref true

let h = Hashtbl.create 17

let () = 
  Hashtbl.add h 0 0;
  Ocamlviz.wait_for_connected_clients 1
let rec fib v w = 
  if v > 0 then fib (v-1) w
  else 0

let f x y = x + y 
;;
let g x = 3
;;

let h arg1 arg2 arg3 arg4 = g arg1 + f arg2 arg3 + arg4

let () =
  while true do
    ignore ( fib (h 1 2 3 4) 5);
  done;
  Ocamlviz.wait_for_killed_clients ()
      
(* -------------- *)
