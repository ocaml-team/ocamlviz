(**************************************************************************)
(*                                                                        *)
(*  Ocamlviz --- real-time profiling tools for Objective Caml             *)
(*  Copyright (C) by INRIA - CNRS - Universite Paris Sud                  *)
(*  Authors: Julien Robert                                                *)
(*           Guillaume Von Tokarski                                       *)
(*           Sylvain Conchon                                              *)
(*           Jean-Christophe Filliatre                                    *)
(*           Fabrice Le Fessant                                           *)
(*  GNU Library General Public License version 2                          *)
(*  See file LICENSE for details                                          *)
(*                                                                        *)
(**************************************************************************)

open Ocamlviz

let int_test = Value.observe_int_ref  "int_test" (ref 0)
  
let () = 
  init ();
  while true do
    int_test := !int_test - (Random.int 2);
    log "%d @." !int_test;
    Unix.sleep 3
  done;
 





(*
Local Variables: 
compile-command: "unset LANG; make -C .. test-graphneg"
End: 
*)
