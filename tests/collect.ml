(**************************************************************************)
(*                                                                        *)
(*  Ocamlviz --- real-time profiling tools for Objective Caml             *)
(*  Copyright (C) by INRIA - CNRS - Universite Paris Sud                  *)
(*  Authors: Julien Robert                                                *)
(*           Guillaume Von Tokarski                                       *)
(*           Sylvain Conchon                                              *)
(*           Jean-Christophe Filliatre                                    *)
(*           Fabrice Le Fessant                                           *)
(*  GNU Library General Public License version 2                          *)
(*  See file LICENSE for details                                          *)
(*                                                                        *)
(**************************************************************************)

open Ocamlviz

let tag = Tag.create "foo"

let () =
  let _ = init () in
  Format.eprintf "wait 1 client@.";
  wait_for_connected_clients 1;
  Format.eprintf "1 client@.";
  let r = ref 0 in
  ignore (Value.observe_int_ref "r" r);
  ignore (Tag.mark tag r);
  for n = 1 to 1_000_000 do
    incr r
  done;
  Format.eprintf "wait kill@.";
  wait_for_killed_clients ();
  Format.eprintf "kill@."

(*
Local Variables: 
compile-command: "unset LANG; make -C .. test-collect"
End: 
*)

