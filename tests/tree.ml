(**************************************************************************)
(*                                                                        *)
(*  Ocamlviz --- real-time profiling tools for Objective Caml             *)
(*  Copyright (C) by INRIA - CNRS - Universite Paris Sud                  *)
(*  Authors: Julien Robert                                                *)
(*           Guillaume Von Tokarski                                       *)
(*           Sylvain Conchon                                              *)
(*           Jean-Christophe Filliatre                                    *)
(*           Fabrice Le Fessant                                           *)
(*  GNU Library General Public License version 2                          *)
(*  See file LICENSE for details                                          *)
(*                                                                        *)
(**************************************************************************)

open Ocamlviz
open Tree
open Protocol

let tree1 = (Protocol.Node ("1",[
			      Protocol.Node ("1.1",[]);
			      Protocol.Node ("1.2",[
					       Protocol.Node ("1.2.1",[
								Protocol.Node ("1.2.1.1", [
										 Protocol.Node ("1.2.1.1.1",[]);
										 Protocol.Node ("1.2.1.1.2",[])
									       ])
							      ])
					     ]);
			      Protocol.Node ("1.3",[
					       Protocol.Node ("1.3.1",[])
					     ]);
			      Protocol.Node ("1.4",[])
			    ]))
let _ = Tree.observe "tree1" (fun () -> tree1)

(* let tree3 = Tree.observe "tree3" (Protocol.Node ("first",[ *)
(* 							Protocol.Node ("second",[]); *)
(* 							Protocol.Node ("third",[]); *)
(* 							Protocol.Node ("fourth",[ *)
(* 									 Protocol.Node ("fifth",[]) *)
(* 								       ]); *)
(* 							Protocol.Node ("sixth",[]) *)
(* 						      ])) *)
  
let () = wait_for_connected_clients 1

let sec = ref 0
  
let b = Node ("b",[]) 

let partage = Node ("a",[b;b])

let d = Node ("d",[])

let e = Node ("e",[b;partage])

let c = Node ("c",[d;e])

let arbre = Protocol.Node ("racine",[c;e])

let mk_tree () =
  let rec fct acc l i =
    if i = 0 then acc
    else
      begin
	match l with
	  |Node (s,f)::r -> fct (Node (s,fct [] f (i-1))::acc) r (i-1)
	  |[] -> acc
      end
  in
  match arbre with
    |Node (s,f) -> Node (s,fct [] f (!sec mod 10))

(* let mk_tree () =  *)
(*   Format.eprintf "sec:%d@." !sec; *)
(*   if !sec < 10 then *)
(*     Node ("<10",[]) *)
(*   else if !sec < 100 then *)
(*     Node ("<10",[Node ("<100",[])]) *)
(*   else if !sec < 1000 then *)
(*     Node ("<10",[Node ("<100",[Node ("<1000",[])])]) *)
(*   else *)
(*     Node ("end",[]) *)

let _ = Tree.observe "arbre" (mk_tree)

let mk_sharing_tree () =
  let rec make = function
    | 0 -> 
	Node ("leaf", [])
    | h -> 
	let t = make (h-1) in 
	Node ("node " ^ string_of_int h, [t;t])
  in
  make (!sec mod 10)

let _ = Tree.observe "tree with sharing" mk_sharing_tree


let mk_sharing_tree2 () = 
  let shared_node = Node ("shared",[Node ("node 6", [])]) in
  Node ("root", [Node ("node 1", [
		    Node ("node 3", []);
		    Node ("node 4", [shared_node])
		  ])
		; Node ("node 2",[
		     Node ("node 5", [shared_node])
		   ]) 
		])

let _ = Tree.observe "sharing" mk_sharing_tree2

let () = 
  while true do incr sec;Unix.sleep 10000 done


(*
Local Variables: 
compile-command: "unset LANG; make -C .. test-tree"
End: 
*)
