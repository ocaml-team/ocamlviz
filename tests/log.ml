(**************************************************************************)
(*                                                                        *)
(*  Ocamlviz --- real-time profiling tools for Objective Caml             *)
(*  Copyright (C) by INRIA - CNRS - Universite Paris Sud                  *)
(*  Authors: Julien Robert                                                *)
(*           Guillaume Von Tokarski                                       *)
(*           Sylvain Conchon                                              *)
(*           Jean-Christophe Filliatre                                    *)
(*           Fabrice Le Fessant                                           *)
(*  GNU Library General Public License version 2                          *)
(*  See file LICENSE for details                                          *)
(*                                                                        *)
(**************************************************************************)

open Ocamlviz 

let cpt = ref 0
let l = ref []

let () = 
  init ();
  wait_for_connected_clients 1;
  while true do
    Unix.sleep 5;
    log ">>>test %d >>>" !cpt;
    for i = 0 to 200 do
    l := !cpt :: !l;
    l := List.rev !l;
    done;
    yield ();
    incr cpt;
  done



(*
Local Variables: 
compile-command: "unset LANG; make -C .. test-log"
End: 
*)

