(**************************************************************************)
(*                                                                        *)
(*  Ocamlviz --- real-time profiling tools for Objective Caml             *)
(*  Copyright (C) by INRIA - CNRS - Universite Paris Sud                  *)
(*  Authors: Julien Robert                                                *)
(*           Guillaume Von Tokarski                                       *)
(*           Sylvain Conchon                                              *)
(*           Jean-Christophe Filliatre                                    *)
(*           Fabrice Le Fessant                                           *)
(*  GNU Library General Public License version 2                          *)
(*  See file LICENSE for details                                          *)
(*                                                                        *)
(**************************************************************************)

open Mlpost
open Num
open Command

let init1 = 
  let node name dx dy fill = Box.round_rect ~name ~dx ~dy ~stroke:(Some Color.black) ~fill (Box.tex name) in
  let program = node "Program" (bp 5.) (bp 29.) Color.lightgreen in
  let ocamlviz = node "Ocamlviz" (bp 5.) (bp 30.) Color.yellow in
  let net = node "Net" (bp 5.) (bp 30.) Color.lightblue in
  let database = node "Database" (bp 5.) (bp 30.) Color.lightgray in
  let display = node "Display" (bp 5.) (bp 29.) Color.orange in
  (program,ocamlviz,net,database,display)
  

let archi1 = 
  let program,ocamlviz,net,database,display = init1 in
  let boxstructure = Array.make_matrix 4 2 (Box.empty ()) in
  let boxserver = Array.make_matrix 1 2 (Box.empty ()) in
  let boxclient = Array.make_matrix 1 3 (Box.empty ()) in
  boxserver.(0).(0) <-  program ;
  boxserver.(0).(1) <-  ocamlviz ;
  boxclient.(0).(0) <-  net ;
  boxclient.(0).(1) <-  database ;
  boxclient.(0).(2) <-  display ;
  let server = Box.tabular ~name:"server" boxserver in
  let client = Box.tabular ~name:"client" boxclient in
  let clientn = Box.tabular ~name:"clientn" boxclient in
  boxstructure.(0).(0) <- Box.tex "Server";
  boxstructure.(0).(1) <- Box.tex "Clients";
  boxstructure.(1).(0) <- server ;
  boxstructure.(1).(1) <- client ;
  boxstructure.(2).(1) <- Box.rect ~name:"etc" ~stroke:None ~dy:(bp 30.) (Box.tex ".  .  .") ;
  boxstructure.(3).(1) <- clientn ;
  let structure = Box.tabular ~vpadding:(bp 10.) ~hpadding:(bp 100.) boxstructure in
  let p1 = Box.east (Box.get "server" structure) in
  let p2 = Box.west (Box.get "client" structure) in
  let p3 = Box.west (Box.get "etc" structure) in
  let p4 = Box.west (Box.get "clientn" structure) in
  let xp1 = Point.xpart p1 in
  let yp1 = Point.ypart p1 in
  let xpic = Num.addn (bp 15.) xp1 in
  let ypic = Num.addn yp1 (bp 10.) in
  let pic = Picture.shift (Point.pt (xpic,ypic)) (Picture.tex "Binary Protocol") in
  let a1 = Arrow.simple (Path.pathp [p1;p2]) in
  let a2 = Arrow.simple (Path.pathp [p1;p3]) in
  let a3 = Arrow.simple (Path.pathp [p1;p4]) in
  (Box.draw structure)++a1++a2++a3++(Picture.make pic)




let _ =
  List.iter (fun (name,fig) -> Metapost.emit name fig)
    [
      "archi",archi1;
    ]
